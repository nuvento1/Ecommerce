import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/core/services/authentication/authentication.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent {

	public loginForm: FormGroup;
	userNameFeedBackMessage: string = "";
	passwordFeedBackMessage: string = "";

	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		private authService: AuthenticationService
	) { }

	ngOnInit(): void {
		this.prepareLoginForm();
	}

	prepareLoginForm() {
		this.loginForm = this.formBuilder.group({
			username: ["", Validators.required],
			password: ["", Validators.required]
		})
	}

	onLoginClick() {
		if (!this.loginValidation()) return;
		const username = this.loginForm.value.username;
		const password = this.loginForm.value.password;
		this.authService.login(username, password).subscribe(res => {
			if (res.length) {
				this.authService.userSubject.next(res[0]);
				this.router.navigate(['admin/dashboard']);
			} else {
				alert("User not found!!!");
			}
		}, err => {
			console.log("Something went wrong!!!");
		})
	}

	loginValidation(): boolean {
		const usernameControl = this.loginForm.get('username');
		const passwordControl = this.loginForm.get('password');
		this.userNameFeedBackMessage = usernameControl?.valid ? "" : "Please enter your Username";
		this.passwordFeedBackMessage = passwordControl?.valid ? "" : "Please enter your Password";

		if (!usernameControl?.valid || !passwordControl?.valid) return false;
		else return true;
	}
}