import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingRoutingModule, routingComponents } from './landing-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
	declarations: [
		routingComponents
	],
	imports: [
		CommonModule,
		LandingRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule
	]
})
export class LandingModule { }
