import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

	constructor(
		private router: Router
	) { }

	admincomponents = [
		{ name: "Attribute", path: "admin/attribute" },
		{ name: "Category", path: "admin/category" },
		{ name: "Product", path: "admin/product" }
	]

	clickedButton(path: string) {
		this.router.navigate([path]);
	}

}
