import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AttributeService } from 'src/app/core/services/attribute/attribute.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.scss']
})
export class AddAttributeComponent {

  public addAttributeForm: FormGroup;
  attributeNameFeedbackMessage: string = "";

  constructor(
    private formBuilder: FormBuilder,
    private attributeService: AttributeService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.prepareAddForm();
  }

  prepareAddForm() {
    this.addAttributeForm = this.formBuilder.group({
      name: ['', Validators.required]
    })
  }

  onSaveClick() {
    if (!this.addAttributeFormValidation()) return;
    const randomKey = Math.floor(Math.random() * 10000);
    const data = {
      id: randomKey,
      name: this.addAttributeForm.value.name,
      values: []
    }
    this.attributeService.addAttribute(data).subscribe(res => {
      this.router.navigate([`admin/attribute/edit/${res.id}`]);
    }, err => {
      alert("Something went wrong!!!");
    })
  }

  addAttributeFormValidation(): boolean {
    const nameControl = this.addAttributeForm.get('name');
    this.attributeNameFeedbackMessage = nameControl?.valid ? "" : "Please enter Attribute Name";
    if (!nameControl?.valid) return false;
    else return true;
  }

  backToAttribute() {
    this.router.navigate(['admin/attribute']);
  }
}
