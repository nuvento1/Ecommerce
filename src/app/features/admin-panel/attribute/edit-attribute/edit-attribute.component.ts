import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AttributeService } from 'src/app/core/services/attribute/attribute.service';
import { Attribute } from 'src/app/shared/models/attribute.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-attribute',
  templateUrl: './edit-attribute.component.html',
  styleUrls: ['./edit-attribute.component.scss']
})
export class EditAttributeComponent {

  attributeId: number;
  attribute?: Attribute;
  public editAttributeForm: FormGroup;
  attributeNameFeedbackMessage: string = "";
  dialogVisible: boolean = false;
  deleteId: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private attributeService: AttributeService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.attributeId = Number(this.route.snapshot.params['id']);
    this.prepareEditForm();
    if (this.attributeId === undefined || this.attributeId === null) {
      this.router.navigate(['admin/attribute']);
      return;
    } else {
      this.getAttribute();
    }
  }

  prepareEditForm() {
    this.editAttributeForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      values: [''],
    });
  }

  getAttribute() {
    this.attributeService.getAttribute(this.attributeId).subscribe((res: Attribute) => {
      this.attribute = res
      this.editAttributeForm.patchValue({
        id: this.attribute?.id,
        name: this.attribute?.name,
        values: '',
      });
    });
  }

  onEditClick() {
    if (!this.editAttributeFormValidation()) return;
    if (this.editAttributeForm.value.values != "") {
      this.attribute?.values.push(this.editAttributeForm.value.values);
    }
    const data = {
      id: this.editAttributeForm.value.id,
      name: this.editAttributeForm.value.name,
      values: this.attribute?.values,
    }
    this.attributeService.updateAttribute(data).subscribe((res: any) => {
      this.editAttributeForm.patchValue({
        values: '',
      });
      alert("Attribute Updated!")
    });
  }

  editAttributeFormValidation(): boolean {
    const nameControl = this.editAttributeForm.get('name');
    this.attributeNameFeedbackMessage = nameControl?.valid ? "" : "Please enter Attribute Name";

    if (!nameControl?.valid) return false;
    else return true;
  }

  onFinishClick() {
    this.router.navigate(['admin/attribute']);
  }

  onDeleteClick(id: number) {
    this.dialogVisible = true;
    this.deleteId = id;
  }

  onDeleteConfirmed(confirmed: boolean) {
    if (confirmed) {
      this.attribute?.values.splice(this.deleteId, 1);
      const data = {
        id: this.editAttributeForm.value.id,
        name: this.editAttributeForm.value.name,
        values: this.attribute?.values,
      }
      this.attributeService.updateAttribute(data).subscribe((res: any) => {
        this.editAttributeForm.patchValue({
          values: '',
        });
      });
    }
    this.dialogVisible = false;
  }

  backToAttribute() {
    this.router.navigate(['admin/attribute']);
  }

}
