import { Component } from '@angular/core';
import { AttributeService } from 'src/app/core/services/attribute/attribute.service';
import { Attribute } from 'src/app/shared/models/attribute.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.scss']
})
export class AttributeComponent {

  public attributes?: Attribute[];
  dialogVisible: boolean = false;
  deleteId: number;

  constructor(
    private attributeService: AttributeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getAttributes();
  }

  getAttributes() {
    this.attributeService.getAttributes().subscribe((res: Attribute[]) => {
      this.attributes = res;
    })
  }

  addAttribute() {
    this.router.navigate(['admin/attribute/add']);
  }

  editAttribute(id: number) {
    this.router.navigate([`admin/attribute/edit/${id}`]);
  }

  deleteAttribute(id: number) {
    this.dialogVisible = true;
    this.deleteId = id;
  }

  onDeleteConfirmed(confirmed: boolean) {
    if (confirmed) {
      this.attributeService.deleteAttribute(this.deleteId).subscribe((res: Attribute[]) => {
        this.getAttributes();
      });
    }
    this.dialogVisible = false;
  }

  backToDashboard() {
    this.router.navigate(['admin/dashboard']);
  }

}
