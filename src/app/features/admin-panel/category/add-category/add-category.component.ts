import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { Router } from '@angular/router';
import { Category } from 'src/app/shared/models/category.model';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent {

  public addCategoryForm: FormGroup;
  public categoryOptions: { name: string, value: number }[] = [];
  public categories?: Category[];
  categoryNameFeedbackMessage: string = "";

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.prepareAddForm();
    this.fetchCategories();
  }

  prepareAddForm() {
    this.addCategoryForm = this.formBuilder.group({
      name: ['', Validators.required],
      parent: ['0', Validators.required]
    })
  }

  fetchCategories() {
    this.categoryService.getCategories().subscribe((result: Category[]) => {
      this.categories = result;
      this.categoryOptions = result.map(res => {
        return { value: res.id, name: res.name }
      });
      this.categoryOptions.unshift({ value: 0, name: "No Parent" });
    })
  }

  onSaveClick() {
    if (!this.addCategoryFormValidation()) return;
    const randomKey = Math.floor(Math.random() * 10000);
    const data = {
      id: randomKey,
      name: this.addCategoryForm.value.name,
      parent: this.addCategoryForm.value.parent
    }
    this.categoryService.addCategory(data).subscribe(res => {
      this.router.navigate(['admin/category']);
    }, err => {
      alert("Something went wrong!!!");
    })
  }

  addCategoryFormValidation(): boolean {
    const nameControl = this.addCategoryForm.get('name');
    this.categoryNameFeedbackMessage = nameControl?.valid ? "" : "Please enter Category Name";

    if (!nameControl?.valid) return false;
    else return true;
  }

  backToCategory() {
    this.router.navigate(['admin/category']);
  }
}
