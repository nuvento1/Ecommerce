import { Component } from '@angular/core';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { Category } from 'src/app/shared/models/category.model';
import { Router } from '@angular/router';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.scss']
})
export class CategoryComponent {

	public categories?: Category[];
	dialogVisible: boolean = false;
	deleteId: number;

	constructor(
		private categoryService: CategoryService,
		private router: Router
	) { }

	ngOnInit() {
		this.getCategories();
	}

	getCategories() {
		this.categoryService.getCategories().subscribe((res: Category[]) => {
			this.categories = res;
		})
	}

	addCategory() {
		this.router.navigate(['admin/category/add']);
	}

	editCategory(id: number) {
		this.router.navigate([`admin/category/edit/${id}`]);
	}

	deleteCategory(id: number) {
		this.dialogVisible = true;
		this.deleteId = id;
	}

	onDeleteConfirmed(confirmed: boolean) {
		if (confirmed) {
			this.categoryService.deleteCategory(this.deleteId).subscribe((res: Category[]) => {
				this.getCategories();
			});
		}
		this.dialogVisible = false;
	}

	backToDashboard() {
		this.router.navigate(['admin/dashboard']);
	}
}
