import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { Category } from 'src/app/shared/models/category.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent {

  categoryId: number;
  category?: Category;
  public editCategoryForm: FormGroup;
  public dropdownOptions: { name: string, value: number }[] = [];
  public categories?: Category[];
  categoryNameFeedbackMessage: string = "";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.categoryId = Number(this.route.snapshot.params['id']);
    if (this.categoryId === undefined || this.categoryId === null) {
      this.router.navigate(['admin/category']);
      return;
    } else {
      this.getCategory();
    }
    this.prepareEditForm();
    this.fetchCategories();
  }

  prepareEditForm() {
    this.editCategoryForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      parent: ['', Validators.required],
    });
  }

  getCategory() {
    this.categoryService.getCategory(this.categoryId).subscribe((res: Category) => {
      this.category = res
      this.editCategoryForm.patchValue({
        id: this.category?.id,
        name: this.category?.name,
        parent: this.category?.parent,
      });
    });
  }

  fetchCategories() {
    this.categoryService.getCategories().subscribe((result: Category[]) => {
      this.categories = result;
      this.dropdownOptions = result.map(res => {
        return { value: res.id, name: res.name }
      });
      this.dropdownOptions.unshift({ value: 0, name: "No Parent" });
    })
  }

  onEditClick() {
    if (!this.editCategoryFormValidation()) return;
    const data = {
      id: this.editCategoryForm.value.id,
      name: this.editCategoryForm.value.name,
      parent: this.editCategoryForm.value.parent,
    }
    this.categoryService.updateCategory(data).subscribe((res: any) => {
      this.router.navigate(['admin/category']);
    }, err => console.log("Something went wrong!!!"));
  }

  editCategoryFormValidation(): boolean {
    const nameControl = this.editCategoryForm.get('name');
    this.categoryNameFeedbackMessage = nameControl?.valid ? "" : "Please enter Category Name";

    if (!nameControl?.valid) return false;
    else return true;
  }

  backToCategory() {
    this.router.navigate(['admin/category']);
  }

}

