import { Component } from '@angular/core';
import { ProductService } from 'src/app/core/services/product/product.service';
import { Product } from 'src/app/shared/models/product.model';
import { Router } from '@angular/router';

@Component({
	selector: 'app-product',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.scss']
})
export class ProductComponent {

	public products?: Product[];
	dialogVisible: boolean = false;
	deleteId: number;

	constructor(
		private productService: ProductService,
		private router: Router
	) { }

	ngOnInit() {
		this.getProducts();
	}

	getProducts() {
		this.productService.getProducts().subscribe((res: Product[]) => {
			this.products = res;
		})
	}

	addProduct() {
		this.router.navigate(['admin/product/add']);
	}

	editProduct(id: number) {
		this.router.navigate([`admin/product/edit/${id}`]);
	}

	deleteProduct(id: number) {
		this.dialogVisible = true;
		this.deleteId = id;
	}

	onDeleteConfirmed(confirmed: boolean) {
		if (confirmed) {
			this.productService.deleteProduct(this.deleteId).subscribe((res: Product[]) => {
				this.getProducts();
			});
		}
		this.dialogVisible = false;
	}

	backToDashboard() {
		this.router.navigate(['admin/dashboard']);
	}
}
