import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from 'src/app/core/services/product/product.service';
import { Router } from '@angular/router';
import { Product } from 'src/app/shared/models/product.model';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { Category } from 'src/app/shared/models/category.model';
import { AttributeService } from 'src/app/core/services/attribute/attribute.service';
import { Attribute } from 'src/app/shared/models/attribute.model';

@Component({
	selector: 'app-add-product',
	templateUrl: './add-product.component.html',
	styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent {

	public addProductForm: FormGroup;
	public categoryOptions: { name: string, value: number }[] = [];
	public attributeOptions: { name: string, value: number }[] = [];
	public products?: Product[];
	productNameFeedbackMessage: string = "";

	constructor(
		private formBuilder: FormBuilder,
		private productService: ProductService,
		private categoryService: CategoryService,
		private attributeService: AttributeService,
		private router: Router
	) { }

	ngOnInit(): void {
		this.prepareAddForm();
		this.fetchCategories();
		this.fetchAttributes();
	}

	prepareAddForm() {
		this.addProductForm = this.formBuilder.group({
			name: ['', Validators.required],
			description: [''],
			categoryId: ['', Validators.required],
			attribute: ['', Validators.required],
		})
	}

	fetchCategories() {
		this.categoryService.getCategories().subscribe((result: Category[]) => {
			this.categoryOptions = result.map(res => {
				return { value: res.id, name: res.name }
			});
			this.categoryOptions.unshift({ value: 0, name: "None" });
		});
	}

	fetchAttributes() {
		this.attributeService.getAttributes().subscribe((result: Attribute[]) => {
			this.attributeOptions = result.map(res => {
				return { value: res.id, name: res.name }
			});
			this.attributeOptions.unshift({ value: 0, name: "None" });
		});
	}

	onSaveClick() {
		if (!this.addProductFormValidation()) return;
		const randomKey = Math.floor(Math.random() * 10000);
		const data = {
			id: randomKey,
			name: this.addProductForm.value.name,
			description: this.addProductForm.value.description,
			categoryId: this.addProductForm.value.categoryId,
			attributes: this.addProductForm.value.attribute
		}
		this.productService.addProduct(data).subscribe(res => {
			this.router.navigate(['admin/product']);
		}, err => {
			alert("Something went wrong!!!");
		})
	}

	addProductFormValidation(): boolean {
		const nameControl = this.addProductForm.get('name');
		this.productNameFeedbackMessage = nameControl?.valid ? "" : "Please enter Category Name";

		if (!nameControl?.valid) return false;
		else return true;
	}

	backToProduct() {
		this.router.navigate(['admin/product']);
	}
}

