import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/core/services/product/product.service';
import { Product } from 'src/app/shared/models/product.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Category } from 'src/app/shared/models/category.model';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { Attribute } from 'src/app/shared/models/attribute.model';
import { AttributeService } from 'src/app/core/services/attribute/attribute.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent {
  productId: number;
  product?: Product;
  public editProductForm: FormGroup;
  public products?: Product[];
  public categoryOptions: { name: string, value: number }[] = [];
  public attributeOptions: { name: string, value: number }[] = [];
  productNameFeedbackMessage: string = "";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private attributeService: AttributeService
  ) { }

  ngOnInit() {
    this.productId = Number(this.route.snapshot.params['id']);
    if (this.productId === undefined || this.productId === null) {
      this.router.navigate(['admin/product']);
      return;
    } else {
      this.getProduct();
    }
    this.prepareEditForm();
    this.fetchCategories();
    this.fetchAttributes();
  }

  prepareEditForm() {
    this.editProductForm = this.formBuilder.group({
      // id: [{ value: '', disabled: true }],
      id: [''],
      name: ['', Validators.required],
      description: [''],
      categoryId: ['', Validators.required],
      attributes: ['', Validators.required],
    });
  }

  getProduct() {
    this.productService.getProduct(this.productId).subscribe((res: Product) => {
      this.product = res
      this.editProductForm.patchValue({
        id: this.product?.id,
        name: this.product?.name,
        description: this.product?.description,
        categoryId: this.product?.categoryId,
        attributes: this.product?.attributes,
      });
    });
  }

  fetchCategories() {
    this.categoryService.getCategories().subscribe((result: Category[]) => {
      this.categoryOptions = result.map(res => {
        return { value: res.id, name: res.name }
      });
      this.categoryOptions.unshift({ value: 0, name: "None" });
    });
  }

  fetchAttributes() {
    this.attributeService.getAttributes().subscribe((result: Attribute[]) => {
      this.attributeOptions = result.map(res => {
        return { value: res.id, name: res.name }
      });
      this.attributeOptions.unshift({ value: 0, name: "None" });
    })
  }

  onEditClick() {
    if (!this.editProductFormValidation()) return;
    const data = {
      id: this.editProductForm.value.id,
      name: this.editProductForm.value.name,
      description: this.editProductForm.value.description,
      categoryId: this.editProductForm.value.categoryId,
      attributes: this.editProductForm.value.attributes
    }
    this.productService.updateProduct(data).subscribe((res: any) => {
      this.router.navigate(['admin/product']);
    }, err => console.log("Something went wrong!!!"));
  }

  editProductFormValidation(): boolean {
    const nameControl = this.editProductForm.get('name');
    this.productNameFeedbackMessage = nameControl?.valid ? "" : "Please enter Category Name";

    if (!nameControl?.valid) return false;
    else return true;
  }

  backToProduct() {
    this.router.navigate(['admin/product']);
  }

}


