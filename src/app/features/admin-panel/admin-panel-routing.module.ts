import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AttributeComponent } from './attribute/attribute.component';
import { AddAttributeComponent } from './attribute/add-attribute/add-attribute.component';
import { EditAttributeComponent } from './attribute/edit-attribute/edit-attribute.component';

import { CategoryComponent } from './category/category.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';

import { ProductComponent } from './product/product.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { EditProductComponent } from './product/edit-product/edit-product.component';

const routes: Routes = [
	{ path: 'dashboard', component: DashboardComponent },

	{ path: 'attribute', component: AttributeComponent },
	{ path: 'attribute/add', component: AddAttributeComponent },
	{ path: 'attribute/edit/:id', component: EditAttributeComponent },

	{ path: 'category', component: CategoryComponent },
	{ path: 'category/add', component: AddCategoryComponent },
	{ path: 'category/edit/:id', component: EditCategoryComponent },

	{ path: 'product', component: ProductComponent },
	{ path: 'product/add', component: AddProductComponent },
	{ path: 'product/edit/:id', component: EditProductComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AdminPanelRoutingModule { }

export const routingComponents = [
	DashboardComponent,

	AttributeComponent,
	AddAttributeComponent,
	EditAttributeComponent,

	CategoryComponent,
	AddCategoryComponent,
	EditCategoryComponent,

	ProductComponent,
	AddProductComponent,
	EditProductComponent,
];
