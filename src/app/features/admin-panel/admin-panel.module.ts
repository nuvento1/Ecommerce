import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelRoutingModule, routingComponents } from './admin-panel-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		routingComponents
	],
	imports: [
		CommonModule,
		AdminPanelRoutingModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule
	]
})
export class AdminPanelModule { }
