import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: 'login',
		loadChildren: () => import('./features/landing/landing.module').then(m => m.LandingModule)
	},
	{
		path: 'admin',
		loadChildren: () => import('./features/admin-panel/admin-panel.module').then(m => m.AdminPanelModule)
	},
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: '**',
		pathMatch: 'full',
		redirectTo: 'page-not-found'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
