import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttributeService } from './services/attribute/attribute.service';
import { AuthenticationService } from './services/authentication/authentication.service'

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AttributeService,
    AuthenticationService
  ]
})
export class CoreModule { }
