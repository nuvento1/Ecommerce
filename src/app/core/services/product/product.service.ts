import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from 'src/app/shared/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get<Product[]>('http://localhost:3000/product');
  }

  getProduct(id: number) {
    return this.http.get<Product>(`http://localhost:3000/product/${id}`);
  }

  addProduct(data: any) {
    return this.http.post<any>('http://localhost:3000/product', data);
  }

  updateProduct(data: any) {
    return this.http.put<any>(`http://localhost:3000/product/${data.id}`, data);
  }

  deleteProduct(id: Number) {
    return this.http.delete<any>(`http://localhost:3000/product/${id}`);
  }
}
