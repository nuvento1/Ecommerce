import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Attribute } from 'src/app/shared/models/attribute.model';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  constructor(private http: HttpClient) { }

  getAttributes() {
    return this.http.get<Attribute[]>('http://localhost:3000/attribute');
  }

  getAttribute(id: number) {
    return this.http.get<Attribute>(`http://localhost:3000/attribute/${id}`);
  }

  addAttribute(data: any) {
    return this.http.post<any>('http://localhost:3000/attribute', data);
  }

  updateAttribute(data: any) {
    return this.http.put<any>(`http://localhost:3000/attribute/${data.id}`, data);
  }

  deleteAttribute(id: Number) {
    return this.http.delete<any>(`http://localhost:3000/attribute/${id}`);
  }

}
