import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  public userSubject = new BehaviorSubject<User>({});

  get user(): Observable<User | null> {
    return this.userSubject.asObservable();
  }

  login(username: string, password: string) {
    return this.http.get<any>(`http://localhost:3000/user/?username=${username}&password=${password}`);
  }

}
