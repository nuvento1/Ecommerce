import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/shared/models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<Category[]>('http://localhost:3000/category');
  }

  getCategory(id: number) {
    return this.http.get<Category>(`http://localhost:3000/category/${id}`);
  }

  addCategory(data: any) {
    return this.http.post<any>('http://localhost:3000/category', data);
  }

  updateCategory(data: any) {
    return this.http.put<any>(`http://localhost:3000/category/${data.id}`, data);
  }

  deleteCategory(id: Number) {
    return this.http.delete<any>(`http://localhost:3000/category/${id}`);
  }
}
