import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-custom-confirm-box',
	templateUrl: './custom-confirm-box.component.html',
	styleUrls: ['./custom-confirm-box.component.scss']
})
export class CustomConfirmBoxComponent {

	@Input() title: string = "";
	@Input() message: string = ""
	@Input() isVisible: boolean = false;
	@Output() confirm = new EventEmitter<boolean>();

	confirmClicked() {
		this.confirm.emit(true);
	}

	cancelClicked() {
		this.confirm.emit(false);
	}

}
