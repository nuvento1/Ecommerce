import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomConfirmBoxComponent } from './custom-confirm-box.component';

describe('CustomConfirmBoxComponent', () => {
  let component: CustomConfirmBoxComponent;
  let fixture: ComponentFixture<CustomConfirmBoxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomConfirmBoxComponent]
    });
    fixture = TestBed.createComponent(CustomConfirmBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
