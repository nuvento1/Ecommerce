import { Component } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

	constructor(
		private authService: AuthenticationService,
		private router: Router
	) { }

	public user: any;

	ngOnInit() {
		this.authService.user.subscribe((value) => this.user = value)
	}

	onLogoutClicked() {
		this.authService.userSubject.next({});
		this.router.navigate(['']);
	}

}
