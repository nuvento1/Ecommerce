export class Category {
	id: number;
	name: string;
	parent: Category;

	constructor(id: number, name: string, parent: Category) {
		this.id = id;
		this.name = name;
		this.parent = parent;
	}
}