import { Attribute } from "./attribute.model";
import { Category } from "./category.model";
export class Product {
	id: number;
	name: string;
	description: string;
	categoryId: Category;
	attributes: Attribute;

	constructor(
		id: number,
		name: string,
		description: string,
		categoryId: Category,
		attributes: Attribute,
	) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.categoryId = categoryId;
		this.attributes = attributes;
	}
}