import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'app-input-textarea',
	templateUrl: './input-textarea.component.html',
	styleUrls: ['./input-textarea.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => InputTextareaComponent),
			multi: true,
		},
	],
})
export class InputTextareaComponent implements ControlValueAccessor {

	@Input() value: string = "";
	@Input() placeholder: string = "";
	@Input() label: string = "";
	@Input() rows: number = 4;
	@Input() cols: number = 50;
	@Output() valueChange = new EventEmitter<string>();

	onChange: any = () => { };
	onTouched: any = () => { };

	writeValue(value: string): void {
		this.value = value;
	}

	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

	onInput(event: Event): void {
		this.value = (event.target as HTMLInputElement).value;
		this.onChange(this.value);
		this.onTouched();
	}

}
