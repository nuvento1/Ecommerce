import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'app-input-dropdown',
	templateUrl: './input-dropdown.component.html',
	styleUrls: ['./input-dropdown.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => InputDropdownComponent),
			multi: true,
		},
	],
})
export class InputDropdownComponent implements ControlValueAccessor {

	@Input() options: any[] = [];
	@Input() label: string = "";
	selectedValue: any;

	writeValue(value: any) {
		this.selectedValue = value;
	}

	registerOnChange(fn: any) {
		this.onChange = fn;
	}

	registerOnTouched(fn: any) {
		this.onTouched = fn;
	}

	selectValue(option: any) {
		this.selectedValue = option;
		this.onChange(option);
		this.onTouched();
	}

	private onChange: (value: any) => void = () => { };
	private onTouched: () => void = () => { };
}