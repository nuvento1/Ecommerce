import { Component, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
	selector: 'app-input-text',
	templateUrl: './input-text.component.html',
	styleUrls: ['./input-text.component.scss'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => InputTextComponent),
		multi: true
	}],
})
export class InputTextComponent implements ControlValueAccessor {

	@Input() label: string = "";
	@Input() type: string = "text"
	@Input() placeholder: string = ""
	@Input() value: string = "";
	@Input() disabled: boolean = false;
	@Input() feedbackMessage: string = ""
	@Output() valueChange = new EventEmitter<string>();

	private onChange: (value: string) => void = () => { };
	private onTouched: () => void = () => { };

	onInput(event: Event): void {
		this.value = (event.target as HTMLInputElement).value;
		this.onChange(this.value);
		this.onTouched();
	}

	writeValue(value: any): void {
		this.value = value;
		this.valueChange.emit(value);
	}

	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

}
