import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextComponent } from './input-components/input-text/input-text.component';
import { InputDropdownComponent } from './input-components/input-dropdown/input-dropdown.component';
import { InputButtonComponent } from './input-components/input-button/input-button.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule } from '@angular/forms';
import { InputTextareaComponent } from './input-components/input-textarea/input-textarea.component';
import { CustomConfirmBoxComponent } from './components/custom-confirm-box/custom-confirm-box.component';

@NgModule({
	declarations: [
		InputTextComponent,
		InputDropdownComponent,
		InputButtonComponent,
		HeaderComponent,
		FooterComponent,
		InputTextareaComponent,
		CustomConfirmBoxComponent
	],
	imports: [
		CommonModule,
		FormsModule
	],
	exports: [
		InputButtonComponent,
		InputTextComponent,
		InputDropdownComponent,
		HeaderComponent,
		FooterComponent,
		InputTextareaComponent,
		CustomConfirmBoxComponent
	]
})
export class SharedModule { }
