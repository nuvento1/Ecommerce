Installation Instructions:

1. Clone the project:
Run the following command to clone the repository:
git clone "https://gitlab.com/nuvento1/Ecommerce.git"

2. Navigate to the "Ecommerce" folder and run the following command to install project dependencies:
npm install

3. To start the development server, run the following command:
ng serve

4. Open a new tab and run following command in the project folder to install json server globally.
npm install -g json-server

5. Start the JSON Server, which serves as the mock database, using the following command:
json-server --watch mock-db.json

6. For admin login, use the following credentials:
Username: "admin"
Password: "admin"

----------------------

Application Flow:

After admin login, a dashboard with buttons for navigating to attribute, category, and product pages is displayed.
Each button directs to a listing page.
A navigation link at the top of all pages allows returning to the previous page.
Adding a new attribute redirects to the edit attribute page, where multiple attribute values can be added.
Adding or updating products and categories redirects to the listing page with the updated values.

----------------------

Project Overview:

Reactive forms with basic validation are used.
Included a sample login page to implement state management using RxJS. Once the user logs in, their name and a logout option will be displayed in the header.
Components have been organized into two modules to implement lazy loading
1. Admin Panel Module: Contains admin-related components.
2. Login Module
Reusable components have been created for confirm dialogue box and input elements, including buttons, text, dropdowns, and text area.
A header and footer have been created to enhance the application's appearance.
A theme file in the styles folder is available for reusing themes, and Flexbox has been used for styling.

-------------------------

Todo:

1. Accidentally, the project was developed using Angular 16. When attempting to downgrade, issues arose.
2. Display the 'Product,' 'Category,' and 'Attribute' buttons as a navigation bar on all admin pages.






